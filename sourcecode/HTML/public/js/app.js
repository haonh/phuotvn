// trim with patterns
String.prototype.trim = function (patterns) {
	patterns = patterns || new RegExp(/\s\uFEFF\xA0/);
	var trimPatterns = new RegExp('^[' + patterns + ']+|[' + patterns + ']+$', 'g');
	return this.replace(trimPatterns, '');
};

// polyfill if need in IE
if (!window.location.origin) {
	window.location.origin = window.location.protocol + "//"
	+ window.location.hostname
	+ (window.location.port ? ':' + window.location.port : '');
}

if (!Function.prototype.bind) {
	Function.prototype.bind = function(oThis) {
		if (typeof this !== 'function') {
			// closest thing possible to the ECMAScript 5
			// internal IsCallable function
			throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
		}

		var aArgs   = Array.prototype.slice.call(arguments, 1),
		fToBind = this,
		fNOP    = function() {},
		fBound  = function() {
			return fToBind.apply(this instanceof fNOP
			? this
			: oThis,
			aArgs.concat(Array.prototype.slice.call(arguments)));
		};

		if (this.prototype) {
			// Function.prototype doesn't have a prototype property
			fNOP.prototype = this.prototype;
		}
		
		fBound.prototype = new fNOP();

		return fBound;
	};
}

function detectIE() {
	var ua = window.navigator.userAgent;

	// Test values; Uncomment to check result …

	// IE 10
	// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

	// IE 11
	// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

	// Edge 12 (Spartan)
	// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

	// Edge 13
	// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// Edge (IE 12+) => return version number
		return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}

	// other browser
	return false;
}

(function ($) {
	var lazyEventHandle = (function() {
		var timer = {};
		return function(callback, time, name) {
			name = name || 'lazyEvent';
			if (timer[name]) {
				clearTimeout(timer[name]);
			}
			timer[name] = setTimeout(callback, time);
		}
	})();

	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function offset(el) {
		var rect = el.getBoundingClientRect(),
		scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
		scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
	}

	function openNav(body, closeMobileNav) {
		body.addClass('open-nav');
		if (body.find('.nav-overlay').length <= 0) {
			body.append('<div class="overlay-mobile"></div>');
		}
		var backDrops = body.find('.overlay-mobile');
		backDrops.one('click', closeMobileNav);
	}

	function windowSize() {
		var width = window.innerWidth
			|| document.documentElement.clientWidth
			|| document.body.clientWidth;

		var height = window.innerHeight
			|| document.documentElement.clientHeight
			|| document.body.clientHeight;
		
		return {
			width: width,
			height: height
		};
	}

	$(document).ready(function() {
		var mobileOpenMenuButton = $('.btn-mobile-menu.tvn-open-menu'),
			mobileCloseMenuButton = $('.btn-mobile-menu.tvn-close-menu'),
			mobileMainMenu = $('#mobile_mainmenu'),
			mobileSearchButton = $('#mobileSearch'),
			mobileSearchBlock = $('.mobileSearch'),
			body = $('body');

		var closeMobileNav = function(e) {
			e.preventDefault();
			var backDrops = body.find('.overlay-mobile');
			body.removeClass('open-nav');
			backDrops.remove('.overlay-mobile');

			if(mobileCloseMenuButton.is(':visible')) {
				mobileCloseMenuButton.hide();
				mobileOpenMenuButton.show();
			}
		};


		mobileOpenMenuButton.on('click', function(e) {
			e.preventDefault();
			openNav(body, closeMobileNav);
			mobileOpenMenuButton.hide();
			mobileCloseMenuButton.show();
		});

		mobileCloseMenuButton.on('click', function(e) {
			e.preventDefault();
			closeMobileNav(e);
			mobileCloseMenuButton.hide();
			mobileOpenMenuButton.show();
		});

		mobileSearchButton.on('click', function(e) {
			var page = body.find('.page-slide');
			var open = mobileSearchBlock.hasClass('open');
			if(open) {
				mobileSearchBlock.removeClass('open');
				if(page) 
					$(page).hide();
			} else {
				if(page) {
					setTimeout(function() {
						$(page).show();
					},100);
					setTimeout(function() {
						mobileSearchBlock.addClass('open');
					}, 500);
				}
			}
		});

		var loginStateEl = $('#loginState');

		// login
		var loginForm = $('#frmLogin'),
			loginButton = $('#btnLogin');

		var email = loginForm.find('#email'),
			password = loginForm.find('#password');

		loginButton.on('click', function() {
			var state = loginStateEl.val();
			if(!state.length) {
				var username = $('#lgn_username').val().trim();
				var password = $('#lgn_username').val().trim();

				if(username == 'admin' && password == 'admin') {
					$('ul.tvn-auth').hide();
					loginStateEl.val('admin');
					console.log('ac');
					window.location.href = "index.html";
				}
			}
		});
	});

	$(window).scroll(function() {
		var backgroundHeight = $('#tvn_background').outerHeight();
		var contentHeight = offset.top;
		var scrollHeight = $(this).scrollTop();

		if(scrollHeight >= backgroundHeight) {
			$('nav#sub-nav').addClass('is-fixed');
		} else {
			$('nav#sub-nav').removeClass('is-fixed');
		}
	});

})(jQuery || {});